<?
$page = "articles";
include "functions.php";
?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title><?=$dataPages['title'];?></title>
	<meta name="description" content="<?=$dataPages['description'];?>">
	<meta name="keywords" content="<?=$dataPages['keywords'];?>">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="menu">
		<ul>
			<? include "/menu.php";?>
		</ul>
	</div>
	<div class="header">
		<? include "/header.php";?>
	</div>
	<div class="pages">
		<div class="content">
			<h1><?=$dataPages['header'];?></h1>
			<div class="text">
				<?=$dataPages['text'];?>
			</div>
			<div class="clear"></div>

            <div class="articles">
				<? foreach($dataArticles as $article):?>
                <div class="header-article"><?=$article['header'];?></div>
                <div class="date-article"><?=$article['date'];?></div>
                <div class="text-article">
                    <p><?=$article['text'];?></p>
                </div>
				<?endforeach;?>
            </div>
		</div>
	</div>
	<div class="footer">
		<? include "/footer.php";?>
	</div>
</body>
</html>