<?
$page = "products";
include "functions.php";
?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title><?=$dataPages['title'];?></title>
	<meta name="description" content="<?=$dataPages['description'];?>">
	<meta name="keywords" content="<?=$dataPages['keywords'];?>">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="menu">
		<ul>
			<? include "/menu.php";?>
		</ul>
	</div>
	<div class="header">
		<? include "/header.php";?>
	</div>
	<div class="pages">
		<div class="content">
			<h1><?=$dataPages['header'];?></h1>
			<div class="text">
				<?=$dataPages['text'];?>
			</div>
			<img class="img" src="images/home.jpg" alt="">
			<div class="clear"></div>
			<div class="products">
				<? foreach($dataProducts as $product):?>
				<div class="head"><?=$product['name'];?></div>
				<img src="images/products/<?=$product['img'];?>" alt="" class="img-pr">
				<div class="description">
					<div class="massa"><?=$product['massa'];?></div>
					<div class="srok">Срок хранения: <?=$product['srok_godnosti'];?></div>
					<div class="text-pr">
						<?=$product['text'];?>
					</div>
				</div>
				<div class="clear"></div>
				<? endforeach;?>
			</div>
		</div>
	</div>
	<div class="footer">
		<? include "/footer.php";?>
	</div>
</body>
</html>